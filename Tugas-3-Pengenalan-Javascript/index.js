// soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

var kapital = kedua.substring(8,18).toUpperCase();

console.log(pertama.substring(0, 5).concat(pertama.substring(12,19)).concat(kedua.substring(0, 8)).concat(kedua.substring(8,18).toUpperCase())); // jawaban soal 1

// soal 2
var kataPertama = Number("10");
var kataKedua = Number("2");
var kataKetiga = Number("4");
var kataKeempat = Number("6");

var hasil = (kataKetiga%kataKeempat)+(kataPertama*kataKedua); // jawaban soal 2
console.log(hasil); // jawaban soal 2

// soal 3
var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0, 3); // jawaban soal 3
var kataKedua = kalimat.slice(4,14); // jawaban soal 3
var kataKetiga = kalimat.substr(15,3); // jawaban soal 3
var kataKeempat = kalimat.substring(19,20).concat(kalimat.substr(20,4)); // jawaban soal 3
var kataKelima = kalimat.slice(25); // jawaban soal 3

console.log('Kata Pertama: ' + kataPertama); // jawaban soal 3
console.log('Kata Kedua: ' + kataKedua); // jawaban soal 3
console.log('Kata Ketiga: ' + kataKetiga); // jawaban soal 3
console.log('Kata Keempat: ' + kataKeempat); // jawaban soal 3
console.log('Kata Kelima: ' + kataKelima); // jawaban soal 3