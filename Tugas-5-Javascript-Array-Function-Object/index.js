// soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();
// jawaban soal 1
for (var i = 0; i < daftarHewan.length; i++) {
    var hasilDaftarHewan = daftarHewan[i];
    console.log(hasilDaftarHewan);
}
// soal 2
var data = {name : "Mohammad Luthfi Ismail Valent" , age : 19 , address : "Jalan Terusan Batununggal" , hobby : "Tidur" };
function introduce(data) { // function soal 2
    return ("Nama saya " + data.name + ", umur saya " + data.age + ", alamat saya di " + data.address + ", dan saya punya hobby yaitu " + data.hobby);
};
var perkenalan = introduce(data); // inisiasi
console.log(perkenalan); // jawaban soal 2
// soal 3
// jawaban soal 3
function hitung_huruf_vokal(str) {
    var vokal = ['a','e','i','o','u','A','E','I','O','U'];
    var hasilhitung = 0;

    for (var z = 0; z < vokal.length; z++) {
        if (vokal.includes(str[z])) {
            hasilhitung++;
        }
    }
    return hasilhitung;
}

var hitung_1 = hitung_huruf_vokal("Muhammad");
var hitung_2 = hitung_huruf_vokal("Iqbal");

console.log(hitung_1, hitung_2);
// soal 4
// jawaban soal 4
function hitung(num) {
    return (num * 2) - 2;
}
console.log(hitung(5));